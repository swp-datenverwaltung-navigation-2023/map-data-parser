# Map data parser

## General description

This application is intended to be ran as a Kron-job in Docker. It should run once every 24 hours and creates a road network,
which is based on OSM. OSM provides three types of element: nodes, ways and relations. Each element has a lot of meta data,
as well as tags that describe different features. The map data parser filters most of this data away.

Ways that represent streets are turned into segments and ways that represent buildings or other
objects are not used. Only the start and end nodes of segments are container within the road network and overlapping segments
are being split. The road type is defined by the `highway` tag.

Relations in OSM represent turn restrictions. The map builder considers only some of those, due to the complexity of certain
turn restrictions and the original scope of the project.

More information about the overall project structure can be found in the project report [here](https://git.imp.fu-berlin.de/swp-datenverwaltung-navigation-2023/organisational).

## Installation instructions

First, add an access key to the repo in the `settings.xml` of your Maven installation.

```xml
<servers>
    <server>
        <id>protos</id>
        <configuration>
            <httpHeaders>
                <property>
                    <name>Private-Token</name>
                    <value>glpat-LUzA2H9fVytwNmmC-6MH</value>
                </property>
            </httpHeaders>
        </configuration>
    </server>
</servers>
```

Run `mvn clean install -U` to get all the dependencies and then run the application as you would in your IDE.

The application writes a `roadnetwork.proto` file when it's done. This can then be used with the routing server or the debug
tool. Check the respective documentations for further information.

### Alternatively

You can also use the Docker image and set it up the container in Docker.
