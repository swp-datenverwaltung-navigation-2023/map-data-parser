package map.builder.osm;

import de.fuberlin.navigator.protos.map_builder.RoadCategory;
import map.builder.osm.json.model.OSMJsonWayDto;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.fuberlin.navigator.protos.map_builder.RoadCategory.*;

public class SegmentUtils {
    // this is a hash-map to allow us to check if a node is a start/end node in linear time
    private SegmentUtils() {}
    // associated each node ID to the segment IDs of the segments that have this node in their geometry
    private static final HashMap<Long, Integer> nodeOccurrenceCount = new HashMap<>();
    private static final List<String> drivableRoads = Arrays.asList(
            "motorway",
            "trunk",
            "primary",
            "secondary",
            "tertiary",
            "unclassified",
            "residential",
            "living_street",
            "motorway_link",
            "trunk_link",
            "primary_link",
            "secondary_link",
            "tertiary_link",
            "living_street",
            "road");

    private static final Map<RoadCategory, Integer> speedMap = Map.ofEntries(
            Map.entry(RoadCategory.ROAD_CATEGORY_HIGHWAY, 120),
            Map.entry(RoadCategory.ROAD_CATEGORY_MAIN, 90),
            Map.entry(ROAD_CATEGORY_LOCAL, 50),
            Map.entry(ROAD_CATEGORY_RESIDENTIAL, 30),
            Map.entry(ROAD_CATEGORY_INVALID, 0)
    );
    private static final Pattern patternMaxSpeed = Pattern.compile("^([0-9][\\.0-9]+?)(?:[ ]?(?:km/h|kmh|kph|mph|knots))?$");

    public static boolean isSegmentDrivable(OSMJsonWayDto dto) {
        if (dto.getHighway() == null) {
            return false;
        }

        return drivableRoads.contains(dto.getHighway());
    }

    public static boolean isSegmentOneway(OSMJsonWayDto dto) {
        return dto.isOneway()
                || OSMJsonUtils.isTagValueEqualTo(dto, "highway", "motorway")
                || OSMJsonUtils.isTagValueEqualTo(dto, "junction", "roundabout");
    }

    public static RoadCategory parseRoadType(OSMJsonWayDto dto) {
        if (dto.getHighway() == null) {
            return ROAD_CATEGORY_INVALID;
        }

        return switch (dto.getHighway()) {
            case "motorway", "highway" -> ROAD_CATEGORY_HIGHWAY;
            case "trunk", "primary", "secondary", "motorway_link", "trunk_link", "primary_link", "secondary_link" ->
                RoadCategory.ROAD_CATEGORY_MAIN;
            case "unclassified", "tertiary", "tertiary_link", "track", "road" ->
                RoadCategory.ROAD_CATEGORY_LOCAL;
            case "residential", "living_street" -> RoadCategory.ROAD_CATEGORY_RESIDENTIAL;
            default -> RoadCategory.ROAD_CATEGORY_INVALID;
        };
    }

    public static int getMaxSpeed(OSMJsonWayDto dto) {

        // if there's a max speed tag, use it
        if (dto.getMaxspeed() != null) {
            Matcher matcherMaxSpeed = SegmentUtils.patternMaxSpeed.matcher(dto.getMaxspeed());

            if (matcherMaxSpeed.find()) {
                return Integer.parseInt(matcherMaxSpeed.group(1));
            }
        }

        // if no tag is available, parse the max speed based on road category
        RoadCategory roadCategory = SegmentUtils.parseRoadType(dto);
        return SegmentUtils.speedMap.get(roadCategory);
    }

    public static void noteNodeOccurrences(OSMJsonWayDto element) {
        long[] nodes = element.getNodes();
        long segmentId = element.getOsmId();

        for (long nodeId : nodes) {
            addNodeToNodeMap(nodeId, segmentId);
        }
    }

    private static void addNodeToNodeMap(long nodeId, long segmentId) {
        if (!nodeOccurrenceCount.containsKey(nodeId)) {
            nodeOccurrenceCount.put(nodeId, 0);
        }

        int nodeIdOccurrences = nodeOccurrenceCount.get(nodeId);
        nodeOccurrenceCount.put(nodeId, nodeIdOccurrences + 1);
    }

    public static boolean isNodeContainedInMultipleSegments(long osmId) {
        return nodeOccurrenceCount.containsKey(osmId) && nodeOccurrenceCount.get(osmId) > 1;
    }

    public static ArrayList<ArrayList<Long>> splitGeometry(long[] nodes) {
        ArrayList<ArrayList<Long>> geometry = new ArrayList<>();

        int from = 0;

        for (int i = 0; i < nodes.length; i++) {
            long nodeId = nodes[i];

            // extract node IDs only if the node occurs multiple times and is NOT an end-node
            if ((isNodeContainedInMultipleSegments(nodeId) && i != 0)
                    || i == (nodes.length - 1)) {
                // extract node ids
                ArrayList<Long> geometryNodeIds = extractIdsFromTo(nodes, from, i);
                geometry.add(geometryNodeIds);

                if (from != 0) {
                    System.out.println("Splitting at position " + from + ", nodes:");
                }

                from = i;
            }
        }

        return geometry;
    }

    private static ArrayList<Long> extractIdsFromTo(long[] nodes, int from, int to) {
        ArrayList<Long> extractedIds = new ArrayList<>();

        for (int i = from; i <= to; i++) {
            long nodeId = nodes[i];
            extractedIds.add(nodeId);
        }

        return extractedIds;
    }
}