package map.builder.osm.json.serialization;

import com.google.gson.*;
import map.builder.osm.json.model.OSMJsonDto;

import java.lang.reflect.Type;

public class OSMJsonDtoDeserializer implements JsonDeserializer<OSMJsonDto> {

    public enum Mode {
        TURN_RESTRICTIONS, NODES_AND_WAYS;
    }

    private Mode mode;

    public OSMJsonDtoDeserializer(Mode mode) {
        this.mode = mode;
    }

    @Override
    public OSMJsonDto deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject object = jsonElement.getAsJsonObject();
        OSMJsonDto dto = switch (object.get("type").getAsString()) {
            case "relation" -> new OSMJsonTurnRestrictionDtoDeserializer().deserialize(object, mode);
            case "node"     -> new OSMJsonNodeDtoDeserializer().deserialize(object, mode);
            case "way"      -> new OSMJsonWayDtoDeserializer().deserialize(object, mode);

            default -> null;
        };

        return dto;
    }
}
