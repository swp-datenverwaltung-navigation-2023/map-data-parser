package map.builder.osm.json.serialization;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import map.builder.osm.json.model.OSMJsonNodeDto;

import java.util.Iterator;
import java.util.Map;

import static map.builder.osm.json.serialization.OSMJsonDtoDeserializer.Mode.NODES_AND_WAYS;

public class OSMJsonNodeDtoDeserializer {

    public OSMJsonNodeDto deserialize(JsonObject obj, OSMJsonDtoDeserializer.Mode mode) throws JsonParseException {
        if (!mode.equals(NODES_AND_WAYS)) {
            return null;
        }

        OSMJsonNodeDto dto = new OSMJsonNodeDto();
        dto.setOsmId(obj.get("id").getAsLong());
        dto.setLat(obj.get("lat").getAsFloat());
        dto.setLon(obj.get("lon").getAsFloat());

        return dto;
    }
}
