package map.builder.osm.json.model;

import java.util.Arrays;
import java.util.stream.Collectors;

public class OSMJsonTurnRestrictionDto extends OSMJsonDto {

    private OSMJsonMemberDto[] members;

    public OSMJsonTurnRestrictionDto() {
        this.type = "relation";
    }

    public OSMJsonMemberDto[] getMembers() {
        return members;
    }

    public void setMembers(OSMJsonMemberDto[] members) {
        this.members = members;
    }

    @Override
    public String toJson() {
        String members = Arrays.asList(getMembers())
                .stream()
                .map(OSMJsonMemberDto::toJson)
                .collect(Collectors.joining(", "));
        return "{ type: relation, members: " + members + " }";
    }
}
