package map.builder.osm.json.model;

public abstract class OSMJsonDto {

    protected String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public abstract String toJson();
}
