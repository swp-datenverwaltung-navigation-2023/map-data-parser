package map.builder.osm.json.serialization;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import map.builder.osm.json.model.OSMJsonMemberDto;
import map.builder.osm.json.model.OSMJsonTurnRestrictionDto;

import java.util.Iterator;
import java.util.Map;

import static map.builder.osm.json.serialization.OSMJsonDtoDeserializer.Mode.TURN_RESTRICTIONS;

public  class OSMJsonTurnRestrictionDtoDeserializer {

    public OSMJsonTurnRestrictionDto deserialize(JsonObject obj, OSMJsonDtoDeserializer.Mode mode) throws JsonParseException {
        if (!mode.equals(TURN_RESTRICTIONS)) {
            return null;
        }

        OSMJsonTurnRestrictionDto dto = new OSMJsonTurnRestrictionDto();
        setMembers(obj.getAsJsonArray("members"), dto);

        return dto;
    }

    public void setMembers(JsonArray elements, OSMJsonTurnRestrictionDto dto) {
        OSMJsonMemberDto[] members = new OSMJsonMemberDto[elements.size()];
        for (int i = 0; i < elements.size(); i++) {
            JsonObject obj = elements.get(i).getAsJsonObject();

            OSMJsonMemberDto member = new OSMJsonMemberDto();
            member.setType(obj.get("type").getAsString());
            member.setRole(obj.get("role").getAsString());
            member.setOsmId(obj.get("ref").getAsLong());

            members[i] = member;
        }

        dto.setMembers(members);
    }
}
