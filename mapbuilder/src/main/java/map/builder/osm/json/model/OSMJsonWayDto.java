package map.builder.osm.json.model;

public class OSMJsonWayDto extends OSMJsonDto {

    private long[] nodes;
    private long osmId;

    private String highway;
    private boolean oneway;
    private String junction;
    private String maxspeed;

    public OSMJsonWayDto() {
        this.type = "way";
    }

    public long[] getNodes() {
        return nodes;
    }

    public void setNodes(long[] nodes) {
        this.nodes = nodes;
    }

    public long getOsmId() {
        return osmId;
    }

    public void setOsmId(long osmId) {
        this.osmId = osmId;
    }

    public String getHighway() {
        return highway;
    }

    public void setHighway(String highway) {
        this.highway = highway;
    }

    public boolean isOneway() {
        return oneway;
    }

    public void setOneway(boolean oneway) {
        this.oneway = oneway;
    }

    public String getJunction() {
        return junction;
    }

    public void setJunction(String junction) {
        this.junction = junction;
    }

    public String getMaxspeed() {
        return maxspeed;
    }

    public void setMaxspeed(String maxspeed) {
        this.maxspeed = maxspeed;
    }

    @Override
    public String toJson() {
        String nodes = "";
        for (long node : this.nodes) {
            nodes = nodes + node + ",";
        }

        return "{ type: way, nodes: [" + nodes + "], osmId: " + osmId + ", highway: " + highway + ", oneway: " + oneway + ", junction: "
                + junction + ", maxspeed: " + maxspeed + "}";
    }
}
