package map.builder.osm.json.model;

public class OSMJsonNodeDto extends OSMJsonDto {

    private long osmId;
    private float lon;
    private float lat;

    public OSMJsonNodeDto() {
        this.type = "node";
    }

    public long getOsmId() {
        return osmId;
    }

    public void setOsmId(long osmId) {
        this.osmId = osmId;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    @Override
    public String toJson() {
        return "{ type: node, osmId: " + osmId + ", lon: " + lon + ", lat: " + lat + " }";
    }
}
