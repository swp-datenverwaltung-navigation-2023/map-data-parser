package map.builder.osm.json.serialization;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import map.builder.osm.json.model.OSMJsonWayDto;

import java.util.Iterator;
import java.util.Map;

import static map.builder.osm.json.serialization.OSMJsonDtoDeserializer.Mode.NODES_AND_WAYS;

public class OSMJsonWayDtoDeserializer {

    public OSMJsonWayDto deserialize(JsonObject obj, OSMJsonDtoDeserializer.Mode mode) throws JsonParseException {
        if (!mode.equals(NODES_AND_WAYS)) {
            return null;
        }

        OSMJsonWayDto dto = new OSMJsonWayDto();
        dto.setOsmId(obj.get("id").getAsLong());
        setNodes(obj.getAsJsonArray("nodes"), dto);
        resolveTags(obj.getAsJsonObject("tags"), dto);

        return dto;
    }

    private void setNodes(JsonArray arr, OSMJsonWayDto dto) {
        long[] nodes = new long[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            nodes[i] = arr.get(i).getAsLong();
        }

        dto.setNodes(nodes);
    }

    private void resolveTags(JsonObject obj, OSMJsonWayDto dto) {
        Iterator<Map.Entry<String, JsonElement>> iterator = obj.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, JsonElement> entry = iterator.next();
            switch (entry.getKey()) {
                case "highway" -> dto.setHighway(entry.getValue().getAsString());
                case "oneway" -> dto.setOneway(entry.getValue().getAsString().equals("yes"));
                case "junction" -> dto.setJunction(entry.getValue().getAsString());
                case "maxspeed" -> dto.setMaxspeed(entry.getValue().getAsString());
            }
        }
    }
}
