package map.builder.osm.json.model;

public class OSMJsonMemberDto extends OSMJsonDto {

    private String role;
    private long osmId;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public long getOsmId() {
        return osmId;
    }

    public void setOsmId(long osmId) {
        this.osmId = osmId;
    }

    @Override
    public String toJson() {
        return "{ type: " + type + ", role: " + role + ", osmId:" + osmId + " }";
    }
}
