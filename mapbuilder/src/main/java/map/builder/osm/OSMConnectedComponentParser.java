package map.builder.osm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import de.fuberlin.navigator.protos.map_builder.Node;
import de.fuberlin.navigator.protos.map_builder.Segment;
import map.builder.utilities.ConnectedComponentGraph;

public class OSMConnectedComponentParser {

    public static void addNodes(Map<Long, Node> nodesMap, ConnectedComponentGraph graph) {
        for (Long nodeId : nodesMap.keySet()) {
            graph.addNode(nodeId);
        }
    }

    public static void addEdges(Map<Long, Segment> segmentsMap, ConnectedComponentGraph graph) {
        for (Long edgeId : segmentsMap.keySet()) {
            Segment segment = segmentsMap.get(edgeId);
            Long startNode = segment.getStartNode();
            Long endNode = segment.getEndNode();
            graph.addEdge(startNode, endNode);
            if (!segment.getOneWay()) {
                graph.addEdge(endNode, startNode);
            }
        }
    }

    public static void cleanUp(Map<Long, Node> nodesMap, Map<Long, Segment> segmentsMap, ArrayList<Long> component) {
        nodesMap.keySet().retainAll(Collections.unmodifiableCollection(component));

        ArrayList<Long> segmentIDsToRemove = new ArrayList<Long>();
        for (Long key : segmentsMap.keySet()) {
            Segment segment = segmentsMap.get(key);
            Long startNodeID = segment.getStartNode();
            Long endNodeID = segment.getEndNode();
            if ((!component.contains(startNodeID)) || (!component.contains(endNodeID))) {
                segmentIDsToRemove.add(key);
            }
        }
        segmentsMap.keySet().removeAll(segmentIDsToRemove);
    }

}