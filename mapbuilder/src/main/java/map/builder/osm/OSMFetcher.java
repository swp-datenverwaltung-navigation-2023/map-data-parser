package map.builder.osm;

import static map.builder.osm.json.serialization.OSMJsonDtoDeserializer.Mode.NODES_AND_WAYS;
import static map.builder.osm.json.serialization.OSMJsonDtoDeserializer.Mode.TURN_RESTRICTIONS;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import map.builder.osm.json.model.OSMJsonDto;
import map.builder.osm.json.serialization.OSMJsonDtoDeserializer;
import map.builder.utilities.BoundingBox;

public class OSMFetcher {
    private final static String OverpassURL = "https://overpass.kumi.systems/api/interpreter";
    private final static String nodeQuery = "[out:json];way[highway](%s);(._;>;);out;";
    private final static String relationQuery = "[out:json];(._;>;);relation[\"type\"=\"restriction\"](%s);(._;>;);out meta;";

    private OSMFetcher() {
    }

    public static List<OSMJsonDto> fetchTurnRestrictions(BoundingBox boundingBox) throws IOException {
        System.out.println("Start to fetch turn restrictions.");
        List<OSMJsonDto> dtos = OSMFetcher.runQueryForBBox(OSMFetcher.relationQuery, boundingBox, TURN_RESTRICTIONS);
        System.out.println("Turn restrictions fetched.");

        return dtos;
    }

    public static List<OSMJsonDto> fetchNodesAndWays(BoundingBox boundingBox) throws IOException {
        System.out.println("Start to fetch nodes and ways.");
        List<OSMJsonDto> dtos = OSMFetcher.runQueryForBBox(OSMFetcher.nodeQuery, boundingBox, NODES_AND_WAYS);
        System.out.println("Nodes and ways fetched.");

        return dtos;
    }

    private static List<OSMJsonDto> runQueryForBBox(String query, BoundingBox bbox, OSMJsonDtoDeserializer.Mode mode)
            throws IOException {
        InputStream response = requestData(query, bbox);

        JsonReader jsonReader = new JsonReader(new InputStreamReader(response));
        List<OSMJsonDto> dtos = new ArrayList<>();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OSMJsonDto.class, new OSMJsonDtoDeserializer(mode))
                .create();

        jsonReader.beginObject();
        String key;
        while ((key = jsonReader.nextName()) != null) {
            if (key.equals("elements")) {
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    OSMJsonDto dto = gson.fromJson(jsonReader, OSMJsonDto.class);

                    if (dto != null) {
                        dtos.add(dto);
                    }
                }

                break;
            }
            jsonReader.skipValue();
        }

        return dtos;
    }

    private static InputStream requestData(String query, BoundingBox bbox) throws IOException {
        URL url = new URL(OverpassURL);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setUseCaches(false);
        connection.setDoOutput(true);

        String queryString = String.format(query, bbox.getQueryString());

        DataOutputStream wr = new DataOutputStream(
                connection.getOutputStream());
        wr.writeBytes(queryString);
        wr.close();

        return connection.getInputStream();
    }

    public static void dumpJsonData(List<OSMJsonDto> data, String path) throws IOException {
        File yourFile = new File(path);
        yourFile.createNewFile();
        FileWriter file = new FileWriter(path);
        file.write(
                "[" +
                        data.stream()
                                .map(OSMJsonDto::toJson)
                                .collect(Collectors.joining(", "))
                        + "]");
        file.close();
    }
}
