package map.builder.osm;

import map.builder.osm.json.model.*;

import java.util.List;
import java.util.Objects;

public class OSMJsonUtils {
    private OSMJsonUtils() {}

    public static boolean isNode(OSMJsonDto dto) {
        return Objects.equals(OSMJsonUtils.getType(dto), "node") && dto instanceof OSMJsonNodeDto;
    }
    public static boolean isWay(OSMJsonDto dto) {
        return Objects.equals(OSMJsonUtils.getType(dto), "way") && dto instanceof OSMJsonWayDto;
    }
    public static boolean isRelation(OSMJsonDto dto) {
        return Objects.equals(OSMJsonUtils.getType(dto), "relation") && dto instanceof OSMJsonTurnRestrictionDto;
    }

    public static boolean isTRFrom(OSMJsonMemberDto dto) {
        return Objects.equals(OSMJsonUtils.getTRType(dto), "from");
    }

    public static boolean isTRTo(OSMJsonMemberDto dto) {
        return Objects.equals(OSMJsonUtils.getTRType(dto), "to");
    }

    public static boolean isTRVia(OSMJsonMemberDto dto) {
        return Objects.equals(OSMJsonUtils.getTRType(dto), "via");
    }

    public static String getType(OSMJsonDto dto) {
        return dto.getType();
    }

    public static String getTRType(OSMJsonMemberDto dto) {
        return dto.getRole();
    }

    public static boolean hasTag(OSMJsonWayDto dto, String tag) {
        return getTag(dto, tag) != null;
    }

    public static String getTag(OSMJsonWayDto dto, String tag) {
        return switch (tag) {
            case "highway" -> dto.getHighway();
            case "junction" -> dto.getJunction();

            default -> null;
        };
    }

    public static boolean isTagValueEqualTo(OSMJsonWayDto dto, String tag, String value) {
        String tagValue = getTag(dto, tag);

        return Objects.equals(tagValue, value);
    }
}
