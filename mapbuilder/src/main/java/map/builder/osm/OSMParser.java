package map.builder.osm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.fuberlin.navigator.protos.map_builder.Coordinates;
import de.fuberlin.navigator.protos.map_builder.Node;
import de.fuberlin.navigator.protos.map_builder.Restriction;
import de.fuberlin.navigator.protos.map_builder.Segment;
import map.builder.osm.json.model.OSMJsonDto;
import map.builder.osm.json.model.OSMJsonMemberDto;
import map.builder.osm.json.model.OSMJsonNodeDto;
import map.builder.osm.json.model.OSMJsonTurnRestrictionDto;
import map.builder.osm.json.model.OSMJsonWayDto;
import map.builder.utilities.ComputationalUtils;

public class OSMParser {
    private final Coordinates.Builder coordinatesBuilder;
    private final HashMap<Long, Node> nodesDump;

    // maps to put the entries

    public HashMap<Long, Node> nodes = new HashMap<Long, Node>();
    public HashMap<Long, Segment> segments = new HashMap<Long, Segment>();
    public ArrayList<Restriction> restrictions = new ArrayList<Restriction>();

    public OSMParser() {
        this.coordinatesBuilder = Coordinates.newBuilder();
        this.nodesDump = new HashMap<>();
    }

    public void parseRoads(List<OSMJsonDto> elements) {
        for (OSMJsonDto dto : elements) {
            if (OSMJsonUtils.isNode(dto)) {
                this.createNode((OSMJsonNodeDto) dto);
            }
        }

        for (OSMJsonDto dto : elements) {
            if (OSMJsonUtils.isWay(dto)) {
                OSMJsonWayDto wayDto = (OSMJsonWayDto) dto;
                if (SegmentUtils.isSegmentDrivable(wayDto)) {
                    SegmentUtils.noteNodeOccurrences(wayDto);
                }
            }
        }

        for (OSMJsonDto dto : elements) {
            if (OSMJsonUtils.isWay(dto)) {
                OSMJsonWayDto wayDto = (OSMJsonWayDto) dto;
                if (SegmentUtils.isSegmentDrivable(wayDto)) {
                    this.splitSegment(wayDto);
                }
            }
        }

    }

    private void splitSegment(OSMJsonWayDto dto) {
        long[] nodes = dto.getNodes();
        long osmId = dto.getOsmId();

        ArrayList<ArrayList<Long>> geometry = SegmentUtils.splitGeometry(nodes);

        if (geometry.size() > 1) {
            System.out.println("Split segment with ID " + osmId + " in " + geometry.size());
        }

        for (int i = 0; i < geometry.size(); i++) {
            this.createSegment(dto, geometry.get(i));
        }
    }

    /**
     * Creates a segment based on the OSM data, however, it ignores the original geometry, if the segment has been split
     * @param dto
     * @param geometry
     */
    private void createSegment(OSMJsonWayDto dto, ArrayList<Long> geometry) {
        long osmId = dto.getOsmId();
        ArrayList<Coordinates> line = null;

        try {
            line = this.findNodePositions(geometry);
        } catch (NullPointerException e) {
            System.out.println("Dropping segment with ID " + osmId);
            System.out.println(e.getMessage());
            return;
        }

        long startNodeId = geometry.get(0);
        long endNodeId = geometry.get(geometry.size() - 1);

        boolean oneWay = SegmentUtils.isSegmentOneway(dto);
        int maxSpeed = SegmentUtils.getMaxSpeed(dto);

        System.out.printf("Max speed: %d \n", maxSpeed);
        System.out.printf("Way id: %d, Start node id : %d, End node id: %d \n", osmId,
                this.nodes.get(startNodeId).getId(),
                this.nodes.get(endNodeId).getId());

        long internalId = this.segments.size();

        Segment segment = Segment.newBuilder()
                .setOsmId(osmId)
                .addAllGeometry(this.findNodePositions(geometry))
                .setOneWay(oneWay)
                .setMaxSpeed(maxSpeed)
                .setId(internalId)
                .setCategory(SegmentUtils.parseRoadType(dto))
                .setStartNode(startNodeId)
                .setEndNode(endNodeId)
                .setLength(this.computeSegmentLength(line))
                .build();

        this.segments.put(internalId, segment);
    }

    private void moveNodeToProto(Node node) {
        long osmId = node.getOsmId();
        this.nodes.put(osmId, node);
    }

    private void createNode(OSMJsonNodeDto dto) {
        Coordinates position = this.createLocationFromElement(dto.getLon(), dto.getLat());

        Node node = Node.newBuilder()
                .setId(this.nodes.size() + this.nodesDump.size())
                .setOsmId(dto.getOsmId())
                .setPosition(position)
                .build();

        this.nodesDump.put(dto.getOsmId(), node);
    }

    private double computeSegmentLength(ArrayList<Coordinates> line) {
        double length = 0;

        for (int i = 1; i < line.size(); i++) {
            Coordinates position_0 = line.get(i);
            Coordinates position_1 = line.get(i - 1);

            length += ComputationalUtils.haversine(position_0, position_1);
        }

        return length;
    }

    /**
     * Returns an array list of array lists of geometries. If the outer list has
     * only one entry, it means the segment
     * doesn't need to be split, otherwise it needs to.
     * 
     * @param nodeIds
     * @return
     * @throws NullPointerException
     */
    private ArrayList<Coordinates> findNodePositions(ArrayList<Long> nodeIds) throws NullPointerException {
        ArrayList<Coordinates> geometry = new ArrayList<>();

        for (int i = 0; i < nodeIds.size(); i++) {
            long currentNodeId = nodeIds.get(i);
            Node currentNode = this.findParsedNodeById(currentNodeId);

            if (currentNode == null) {
                return geometry;
            }

            if (i == 0 || i == nodeIds.size() - 1) {

                // if it is the first or last node, we need to save it to the proto
                moveNodeToProto(currentNode);
            }

            Coordinates position = currentNode.getPosition();
            geometry.add(position);
        }

        return geometry;
    }

    // TODO may need a more appropriate exception
    private Node findParsedNodeById(long osmId) throws NullPointerException {
        if (this.nodesDump.containsKey(osmId)) {
            Node node = this.nodesDump.get(osmId);
            return node;
        }

        if (this.nodes.containsKey(osmId)) {
            return this.nodes.get(osmId);
        }

        throw new NullPointerException("Node with id " + osmId + " not found!");
    }

    private Coordinates createLocationFromElement(float lon, float lat) {
        return coordinatesBuilder
                .setLon(lon)
                .setLat(lat)
                .build();
    }

    public void parseTurnRestrictions(List<OSMJsonDto> elements) {
        for (OSMJsonDto dto : elements) {
            if (OSMJsonUtils.isRelation(dto)) {
                OSMJsonMemberDto[] members = ((OSMJsonTurnRestrictionDto) dto).getMembers();
                if (!hasWayVia(members)) {
                    parseFromTo(members);
                }
            }
        }
    }

    private void parseFromTo(OSMJsonMemberDto[] members) {
        if (!hasWayVia(members)) {
            int from = 0;
            int to = 0;
            for (OSMJsonMemberDto member : members) {
                if (OSMJsonUtils.isTRFrom(member)) {
                    from = (int) member.getOsmId();
                }
                if (OSMJsonUtils.isTRTo(member)) {
                    to = (int) member.getOsmId();
                }
            }
            Restriction restriction = Restriction.newBuilder().setFromId(from).setToId(to).build();
            this.restrictions.add(restriction);
        }
    }

    private boolean hasWayVia(OSMJsonMemberDto[] members) {
        if (members.length < 3) {
            return false;
        }
        for (OSMJsonMemberDto member : members) {
            if (OSMJsonUtils.isTRVia(member)) {
                if (OSMJsonUtils.isWay(member)) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }
}
