package map.builder.utilities;

public class BoundingBox implements BoundingBoxInterface {
    private float minLat = 0;
    private float minLon = 0;
    private float maxLat = 0;
    private float maxLon = 0;

    public BoundingBox(float minLat, float minLon, float maxLat, float maxLon) {
        this.minLat = (minLat < maxLat) ? minLat : maxLat;
        this.maxLat = (minLat < maxLat) ? maxLat : minLat;
        this.minLon = (minLon < maxLon) ? minLon : maxLon;
        this.maxLon = (minLon < maxLon) ? maxLon : minLon;
    }

    @Override
    public float getMinLat() {
        return minLat;
    }

    @Override
    public float getMinLon() {
        return minLon;
    }

    @Override
    public float getMaxLat() {
        return maxLat;
    }

    @Override
    public float getMaxLon() {
        return maxLon;
    }

    @Override
    public String getQueryString() {
        StringBuffer queryString = new StringBuffer("");
        queryString.append(getMinLat());
        queryString.append(",");
        queryString.append(getMinLon());
        queryString.append(",");
        queryString.append(getMaxLat());
        queryString.append(",");
        queryString.append(getMaxLon());
        System.out.println(queryString);
        return queryString.toString();
    }
}