package map.builder.utilities;

interface BoundingBoxInterface {
    public float getMinLat();

    public float getMinLon();

    public float getMaxLat();

    public float getMaxLon();

    public String getQueryString();
}
