package map.builder.utilities;

import java.io.FileOutputStream;
import java.io.IOException;

import de.fuberlin.navigator.protos.map_builder.RoadNetwork;

public class FileHandler {
    public static void saveToFile(RoadNetwork roadnetwork, String filepath) throws IOException {
        FileOutputStream output = new FileOutputStream(filepath);
        roadnetwork.writeTo(output);
        output.close();
    }
}
