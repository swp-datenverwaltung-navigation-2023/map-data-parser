package map.builder.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {

    private Config() {
    }

    private static Properties props = new Properties();

    public static String ROAD_NETWORK_OUTPUT_PATH = "roadnetwork.proto";

    /**
     * Bounding box, default small box inside Cottbus
     */
    public static float BOUNDING_BOX_MIN_LAT = 51.754092326645475f;
    public static float BOUNDING_BOX_MIN_LON = 14.300615062713623f;
    public static float BOUNDING_BOX_MAX_LAT = 51.766591637718435f;
    public static float BOUNDING_BOX_MAX_LON = 14.314413070678711f;

    public static void load() {
        //The config is used for running the application in a Docker container
        String configFilePath = "/data/configuration/config.properties";

        try (FileInputStream is = new FileInputStream(configFilePath)){
            props.load(is);

            ROAD_NETWORK_OUTPUT_PATH = props.getProperty("MAP_BUILDER_ROAD_NETWORK_OUTPUT_PATH", ROAD_NETWORK_OUTPUT_PATH);

            BOUNDING_BOX_MIN_LAT = Float.parseFloat(props.getProperty("BOUNDING_BOX_MIN_LAT", String.valueOf(BOUNDING_BOX_MIN_LAT)));
            BOUNDING_BOX_MIN_LON = Float.parseFloat(props.getProperty("BOUNDING_BOX_MIN_LON", String.valueOf(BOUNDING_BOX_MIN_LON)));
            BOUNDING_BOX_MAX_LAT = Float.parseFloat(props.getProperty("BOUNDING_BOX_MAX_LAT", String.valueOf(BOUNDING_BOX_MAX_LAT)));
            BOUNDING_BOX_MAX_LON = Float.parseFloat(props.getProperty("BOUNDING_BOX_MAX_LON", String.valueOf(BOUNDING_BOX_MAX_LON)));
            System.out.println("Config loaded.");
        } catch (FileNotFoundException e) {
            // Either way the Docker image was build wrong or this is not
            // run in a Docker environment
            System.out.println("No config file found. Using default settings!");
        } catch (IOException e) {
            // Something else went wrong, but we will
            // not let this escalate
            e.printStackTrace();
        }

    }
}
