package map.builder.utilities;

import de.fuberlin.navigator.protos.map_builder.Coordinates;

public class ComputationalUtils {

    public static double haversine(Coordinates position_0,
            de.fuberlin.navigator.protos.map_builder.Coordinates position_1) {
        float lat_0 = position_0.getLat();
        float lon_0 = position_0.getLon();

        float lat_1 = position_1.getLat();
        float lon_1 = position_1.getLon();

        return ComputationalUtils.haversine(lat_0, lon_0, lat_1, lon_1);
    }

    // do this for each node and its successor in the (sorted) node list for each
    // segment and then add it all together
    public static double haversine(float lat1, float lon1, float lat2, float lon2) {
        final int R = 6371000; // Radious of the earth
        float latDistance = toRad(lat2 - lat1);
        float lonDistance = toRad(lon2 - lon1);
        float a = (float) (Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2));
        float c = (float) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
        return R * c;
    }

    private static float toRad(float value) {
        return (float) (value * Math.PI / 180);
    }
}
