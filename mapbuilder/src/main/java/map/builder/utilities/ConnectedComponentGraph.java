package map.builder.utilities;

import java.util.ArrayList;
import java.util.HashMap;
// Code from : https://www.geeksforgeeks.org/strongly-connected-components/
// Java implementation of Kosaraju's algorithm to print all SCCs
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

//TODO: change data structure here to be more appropriate
// This class represents a directed ConnectedComponentGraph using adjacency list
// representation
public class ConnectedComponentGraph {
    private HashMap<Long, LinkedList<Long>> adj; // Adjacency List

    // Constructor
    public ConnectedComponentGraph() {
        adj = new HashMap<Long, LinkedList<Long>>();
    }

    public void addNode(Long v) {
        adj.put(v, new LinkedList<Long>());
    }

    // Function to add an edge into the ConnectedComponentGraph
    public void addEdge(Long v, Long w) {
        adj.get(v).add(w);
    }

    // iterative function for calculating DFS
    public ArrayList<Long> DFS(Long s, HashMap<Long, Boolean> visited) {
        // Create a stack for DFS
        Stack<Long> stack = new Stack<Long>();
        ArrayList<Long> vertices = new ArrayList<Long>();

        // Push the current source node
        stack.push(s);

        while (stack.empty() == false) {
            // Pop a vertex from stack and print it
            s = stack.peek();
            stack.pop();

            // Stack may contain same vertex twice. So
            // we need to print the popped item only
            // if it is not visited.
            if (visited.get(s) == false) {
                visited.put(s, true);
                vertices.add(s);
            }

            // Get all adjacent vertices of the popped vertex s
            // If a adjacent has not been visited, then push it
            // to the stack.
            Iterator<Long> itr = adj.get(s).iterator();

            while (itr.hasNext()) {
                Long v = itr.next();
                if (!visited.get(v))
                    stack.push(v);
            }

        }

        return vertices;
    }

    // Function that returns reverse (or transpose) of this ConnectedComponentGraph
    public ConnectedComponentGraph getTranspose() {
        ConnectedComponentGraph g = new ConnectedComponentGraph();
        for (Long v : adj.keySet()) {
            g.addNode(v);
        }
        for (Long v : adj.keySet()) {
            // Recur for all the vertices adjacent to this vertex
            Iterator<Long> i = adj.get(v).listIterator();
            while (i.hasNext()) {
                Long next = i.next();
                g.adj.get(next).add(v);
            }
        }
        return g;
    }

    public void fillOrder(Long v, HashMap<Long, Boolean> visited, Stack<Long> stack) {
        // Create a stack for filling
        Stack<Long> helperStack = new Stack<Long>();

        // Push the current source node
        helperStack.push(v);
        while (helperStack.empty() == false) {
            // Pop a vertex from stack and print it
            v = helperStack.peek();
            helperStack.pop();
            // Stack may contain same vertex twice. So
            // we need to print the popped item only
            // if it is not visited.
            if (visited.get(v) == false) {
                visited.put(v, true);
            }

            Iterator<Long> itr = adj.get(v).iterator();

            while (itr.hasNext()) {
                Long s = itr.next();
                if (!visited.get(s))
                    helperStack.push(s);
            }

            stack.push(v);
        }
    }

    // The main function that finds and prints all strongly
    // connected components
    public ArrayList<Long> getSCCs() {
        Stack<Long> stack = new Stack<Long>();

        // Mark all the vertices as not visited (For first DFS)
        HashMap<Long, Boolean> visited = new HashMap<Long, Boolean>();
        for (Long i : this.adj.keySet()) {
            visited.put(i, false);
        }

        // Fill vertices in stack according to their finishing
        // times

        for (Long i : this.adj.keySet())
            if (visited.get(i) == false)
                fillOrder(i, visited, stack);

        // Create a reversed ConnectedComponentGraph
        ConnectedComponentGraph gr = getTranspose();

        // Mark all the vertices as not visited (For second DFS)
        for (Long i : this.adj.keySet())
            visited.put(i, false);

        // Now process all vertices in order defined by Stack
        ArrayList<Long> largestComponent = new ArrayList<Long>();
        int maxLength = 0;
        while (stack.empty() == false) {
            // Pop a vertex from stack
            Long v = stack.pop();

            // Print Strongly connected component of the popped vertex
            if (visited.get(v) == false) {
                ArrayList<Long> component = gr.DFS(v, visited);
                if (maxLength < component.size()) {
                    maxLength = component.size();
                    largestComponent = component;
                }
            }
        }
        return largestComponent;
    }

    // Driver method
    public static void main(String args[]) {
        // Create a ConnectedComponentGraph given in the above diagram
        ConnectedComponentGraph g = new ConnectedComponentGraph();
        // add nodes
        g.addNode((long) 0);
        g.addNode((long) 1);
        g.addNode((long) 2);
        g.addNode((long) 3);
        g.addNode((long) 4);

        g.addEdge((long) 1, (long) 0);
        g.addEdge((long) 0, (long) 2);
        g.addEdge((long) 2, (long) 1);
        g.addEdge((long) 0, (long) 3);
        g.addEdge((long) 3, (long) 4);

        System.out.println("Following are strongly connected components " +
                "in given ConnectedComponentGraph ");
        g.getSCCs();
    }
}
// This code is contributed by Aakash Hasija