package map.builder;

import static map.builder.utilities.Config.ROAD_NETWORK_OUTPUT_PATH;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.fuberlin.navigator.protos.map_builder.RoadNetwork;
import map.builder.osm.OSMConnectedComponentParser;
import map.builder.osm.OSMFetcher;
import map.builder.osm.OSMParser;
import map.builder.osm.json.model.OSMJsonDto;
import map.builder.utilities.BoundingBox;
import map.builder.utilities.Config;
import map.builder.utilities.ConnectedComponentGraph;
import map.builder.utilities.FileHandler;

import java.io.IOException;
import java.util.List;

import static map.builder.utilities.Config.*;

public class App {
    public static void main(String[] args) throws IOException {
        System.out.println("Map builder started!");
        Config.load();

        RoadNetwork.Builder roadNetworkBuilder = RoadNetwork.newBuilder();
        OSMParser parser = new OSMParser();

        BoundingBox bbox = new BoundingBox(BOUNDING_BOX_MIN_LAT, BOUNDING_BOX_MIN_LON, BOUNDING_BOX_MAX_LAT, BOUNDING_BOX_MAX_LON);

        List<OSMJsonDto> restrictions = OSMFetcher.fetchTurnRestrictions(bbox);
        List<OSMJsonDto> roads = OSMFetcher.fetchNodesAndWays(bbox);

        System.out.println("Starting to parse.");
        parser.parseTurnRestrictions(restrictions);
        parser.parseRoads(roads);
        System.out.println("Parsed road network.");

        // set it to 0 so that it can be collected by garbage collector
        restrictions = null;
        roads = null;
        bbox = null;

        // create the nodes graph in order to run LCC on it
        ConnectedComponentGraph graph = new ConnectedComponentGraph();
        OSMConnectedComponentParser.addNodes(parser.nodes, graph);
        OSMConnectedComponentParser.addEdges(parser.segments, graph);

        ArrayList<Long> component = graph.getSCCs();

        // cleanup
        graph = null;
        OSMConnectedComponentParser.cleanUp(parser.nodes, parser.segments, component);

        RoadNetwork roadNetwork = roadNetworkBuilder.putAllNodes(parser.nodes).putAllSegments(parser.segments)
                .addAllTurnRestrictions(parser.restrictions).build();
        System.out.println("Turn restrictions count: " + roadNetwork.getTurnRestrictionsCount());
        System.out.println("Nodes count: " + roadNetwork.getNodesCount());
        System.out.println("Segments count: " + roadNetwork.getSegmentsCount());
        FileHandler.saveToFile(roadNetwork, ROAD_NETWORK_OUTPUT_PATH);
    }
}
